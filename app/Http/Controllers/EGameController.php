<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EGameController extends Controller
{
    //選擇廠商畫面
    public function selectGame()
    {
        $user = session('Auth');
        
        // dd(session('Auth'));
        $games = DB::select('CALL sp_Front_GameList');
        $CoinType = DB::select('CALL sp_General_CoinTypeMenu');
        
        // 会员钱包
        $wrs = DB::select('CALL sp_Front_MemberWalletAndMinerRevenue(?)', [
            $user->MemberID
        ]);
        $user->Wallet = $wrs[0];
        session(['Auth' => $user]);

        return view('pages.selectgame', compact('games', 'CoinType'));
    }


    //取得WG遊戲列表
    public function wg_List()
    {
        $user = session('Auth');
        
        // dd(session('Auth'));
        $games = DB::select('CALL sp_Front_GameList');
        $CoinType = DB::select('CALL sp_General_CoinTypeMenu');
    
        // 会员钱包
        $wrs = DB::select('CALL sp_Front_MemberWalletAndMinerRevenue(?)', [
            $user->MemberID
        ]);
        $user->Wallet = $wrs[0];
        session(['Auth' => $user]);

        return view('pages.game', compact('games', 'CoinType'));
    }


    //WG: 進入遊戲
    public function wg_Game(Request $request, $id)
    {
        // echo $request->coin . '<br>';
        
        $errors = [];

        $user = session('Auth');
        
        if ($request->playLeave == 'play') {
            // 取得幣別ID 1: 金幣, 2: 銀幣, 3: 活動幣
            $coin_type  = $request->coinType;
            // 取得金幣、銀幣、活動幣的錢包餘額
            $wallet_rs  = $user->Wallet;

            $url_server = env('CURL_SERVER');
                
            // 先進行登入
            $url_method = $url_server . '/login';
            $data = [
                'user_id' => $user->MemberID
            ];
            $res = curl_post($url_method, json_encode($data));

            //取得錢包餘額並在WG端寫入會員資料
            $url_method = $url_server . '/wg/user/wallet/remote?language=thTH&token=' . $res['token'];
            $data = [];
            $response = curl_get($url_method, json_encode($data));
            // 電子遊戲錢包餘額未大於0,才檢查是否有填入金額
            if ($response['point'] <= 0){
                // 先確認金額是否有填入
                if ($request->coin == 0 || null || ''){
                    $errors = ['message' => '請填入金額'];
                    return redirect('/wg_list')
                        ->withErrors($errors)
                        ->withInput();
                }
            }

            // 確認金額是否為負數
            if ($request->coin < 0){
                $errors = ['message' => '金額不可為負'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認金幣餘額是否足夠
            if ($coin_type == '1' && $wallet_rs->GoldCoin/1000 < $request->coin){
                $errors = ['message' => '金幣餘額不足'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認銀幣餘額是否足夠
            if ($coin_type == '2' && $wallet_rs->SilverCoin/1000 < $request->coin){
                $errors = ['message' => '銀幣餘額不足'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認活動幣餘額是否足夠
            if ($coin_type == '3' && $wallet_rs->ActivityCoin/1000 < $request->coin){
                $errors = ['message' => '活動幣餘額不足'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 有輸入金額才進行轉點
            if ($request->coin > 0){
                // 先進行轉點
                $url_method = $url_server . '/wg/user/wallet/change?language=thTH&token=' . $res['token'];
                
                // 金幣
                if ($coin_type == '1'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->GoldCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 銀幣
                if ($coin_type == '2'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->SilverCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 活動幣
                if ($coin_type == '3'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->ActivityCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 預存進入遊戲
                $rs = DB::select('CALL sp_Front_Game_PlayGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    intval($request->coin) * 1000,
                    now()
                ]);
            }else{
                // 預存進入遊戲
                $rs = DB::select('CALL sp_Front_Game_PlayGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    0,
                    now()
                ]);
            }

            // 進入遊戲
            $url_method = $url_server . '/wg/game/' . $id . '/login?language=thTH&token='.$res['token'];
            $data = [
                'return_url' => 'http://wonder-gaming.com/wg_list',
                'wallet'     => $user->Wallet
            ];
            
            $rs_data = curl_post($url_method, json_encode($data));

            if (isset($rs_data['login_url'])) {
                return redirect($rs_data['login_url']);
        
            } else {
                $errors = ['message' => '遊戲目前維護中'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
            }
        
        // WG: 退出遊戲 
        } else {
            $url_server = env('CURL_SERVER');

            // 先進行登入
            $url_method = $url_server . '/login';
            $data = [
                'user_id' => $user->MemberID
            ];
            $res = curl_post($url_method, json_encode($data));

            // 取得錢包餘額(WG端)
            $url_method = $url_server . '/wg/user/wallet/remote?language=thTH&token=' . $res['token'];
            $data = [];
            $response = curl_get($url_method, json_encode($data));
            
            // 退出遊戲確認WG錢包是否有餘額，有的話直接轉出
            if ( $response['point'] > 0 ) {

                if ($request->coin != 0 || null || ''){
                    $errors = ['message' => '退出遊戲不需輸入金額'];
                    return redirect('/wg_list')
                        ->withErrors($errors)
                        ->withInput();
                }
                
                $url_method = $url_server . '/wg/user/wallet/change?language=thTH&token=' . $res['token'];
                $coin_type  = $request->coinType;
                $wallet_rs  = $user->Wallet;
                // 金幣
                if ($coin_type == '1'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->GoldCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 銀幣
                if ($coin_type == '2'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->SilverCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 活動幣
                if ($coin_type == '3'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->ActivityCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                    
                // 使用資料庫預存 - 退出遊戲，並將WG錢包餘額帶入
                $rs = DB::select('CALL sp_Front_Game_LeaveGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    intval($response['point'])/4 * 1000,
                    now()
                ]);

                $errors = [];
                if ($rs[0]->ResultCode != 0) {
                    if ($rs[0]->ResultCode == 1) {
                        $errors = ['message' => '會員不存在'];
                    }
                    if ($rs[0]->ResultCode == 2) {
                        $errors = ['message' => '遊戲不存在'];
                    }
                    if ($rs[0]->ResultCode == 3) {
                        $errors = ['message' => '幣別不存在'];
                    }
                    if ($rs[0]->ResultCode == 4) {
                        $errors = ['message' => '餘額異常不存在'];
                    }
                    if ($rs[0]->ResultCode == 5) {
                        $errors = ['message' => '金幣不夠'];
                    }
                    if ($rs[0]->ResultCode == 6) {
                        $errors = ['message' => '銀幣不夠'];
                    }
                    if ($rs[0]->ResultCode == 7) {
                        $errors = ['message' => '活動幣不夠'];
                    }
                    if ($rs[0]->ResultCode == 8) {
                        $errors = ['message' => '還在遊戲中'];
                    }
                    if ($rs[0]->ResultCode == 9) {
                        $errors = ['message' => '交易失敗'];
                    }
            
                    return redirect('/wg_list')
                            ->withErrors($errors)
                            ->withInput();
                }
                $errors = ['message' => 'OK'];
                    return redirect('/wg_list')
                        ->withErrors($errors)
                        ->withInput();
            }else{
                // 當WG錢包餘額為0
                $errors = ['message' => '電子遊戲錢包餘額為0'];
                return redirect('/wg_list')
                    ->withErrors($errors)
                    ->withInput();
                
                //取得會員下注紀錄
                // $url_server = env('CURL_SERVER');

                // $url_method = $url_server . '/game/report/user?zone=vaNTD';
                
                // $data = [
                //     'user_name'  => $user->MemberAccount,
                // ];
            
                // $rs_data = curl_post($url_method, json_encode($data));

                // $errors = ['message' => '退出遊戲程序錯誤'];
                            
                // return redirect('/wg_list')
                //     ->withErrors($errors)
                //     ->withInput();  
                
            }
        }
    }


    //取得VA遊戲列表
    public function va_List()
    {
        $user = session('Auth');
        
        $url_server = env('CURL_SERVER');

        // 先進行登入
        $url_method = $url_server . '/login';
        $data = [
            'user_id' => $user->MemberID
        ];
        $res = curl_post($url_method, json_encode($data));
        
        //取得錢包餘額並在WG端寫入會員資料
        $url_method = $url_server . '/wg/user/wallet/remote?language=thTH&token=' . $res['token'];
        $data = [];
        $response = curl_get($url_method, json_encode($data));
        
        //取得遊戲列表
        $url_method = $url_server . '/wg/game/?language=thTH&token=' . $res['token'];
        $data = [];

        $games = curl_get($url_method, json_encode($data));
        
        // for ($item=0;$item<$i;$item++){
        //     $games = $game[$item];
        // };
        
        $games = json_decode(json_encode($games));
        $CoinType = DB::select('CALL sp_General_CoinTypeMenu');
        
        // 会员钱包
        $wrs = DB::select('CALL sp_Front_MemberWalletAndMinerRevenue(?)', [
            $user->MemberID
        ]);
        $user->Wallet = $wrs[0];
        session(['Auth' => $user]);

        return view('pages.va_game', compact('games', 'CoinType'));
    }


    //VA: 進入遊戲
    public function va_Game(Request $request, $id)
    {
        echo $request->coin . '<br>';
        
        $errors = [];

        $user = session('Auth');
        
        if ($request->playLeave == 'play') {

            // 取得幣別ID 1: 金幣, 2: 銀幣, 3: 活動幣
            $coin_type  = $request->coinType;
            // 取得金幣、銀幣、活動幣的錢包餘額
            $wallet_rs  = $user->Wallet;
            
            $url_server = env('CURL_SERVER');
            
            // 先進行登入
            $url_method = $url_server . '/login';
            $data = [
                'user_id' => $user->MemberID
            ];
            $res = curl_post($url_method, json_encode($data));

            //取得錢包餘額並在WG端寫入會員資料
            $url_method = $url_server . '/wg/user/wallet/remote?language=thTH&token=' . $res['token'];
            $data = [];
            $response = curl_get($url_method, json_encode($data));
            // 電子遊戲錢包餘額未大於0,才檢查是否有填入金額
            if ($response['point'] <= 0){
                // 先確認金額是否有填入
                if ($request->coin == 0 || null || ''){
                    $errors = ['message' => 'กรุณากรอกจำนวนเงิน'];
                    return redirect('/va_list')
                        ->withErrors($errors)
                        ->withInput();
                }
            }
            // 確認金額是否為負數
            if ($request->coin < 0){
                $errors = ['message' => 'ยอดเงินไม่ควรน้อยกว่า 0'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認金幣餘額是否足夠
            if ($coin_type == '1' && $wallet_rs->GoldCoin/1000 < $request->coin){
                $errors = ['message' => 'ยอดเหรียญทองไม่เพียงพอ'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認銀幣餘額是否足夠
            if ($coin_type == '2' && $wallet_rs->SilverCoin/1000 < $request->coin){
                $errors = ['message' => 'ยอดเงินเหรียญเงินไม่เพียงพอ'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }
            // 確認活動幣餘額是否足夠
            if ($coin_type == '3' && $wallet_rs->ActivityCoin/1000 < $request->coin){
                $errors = ['message' => 'ยอดเงินเหรียญเล่นเกมไม่เพียงพอ'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }

            // 有輸入金額才進行轉點
            if ($request->coin > 0){
                // 先進行轉點
                $url_method = $url_server . '/wg/user/wallet/change?language=thTH&token=' . $res['token'];
                
                // 金幣
                if ($coin_type == '1'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->GoldCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 銀幣
                if ($coin_type == '2'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->SilverCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 活動幣
                if ($coin_type == '3'){
                    $data = [
                        'point'  => $request->coin,
                        'wallet' => $wallet_rs->ActivityCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 預存進入遊戲
                $rs = DB::select('CALL sp_Front_Game_PlayGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    intval($request->coin) * 1000,
                    now()
                ]);
            }else{
                // 預存進入遊戲
                $rs = DB::select('CALL sp_Front_Game_PlayGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    0,
                    now()
                ]);
            }
            
            // 進入遊戲
            $url_method = $url_server . '/wg/game/' . $id . '/login?language=thTH&token='.$res['token'];
            $data = [
                'return_url' => 'http://wonder-gaming.com/va_list',
                'wallet'     => $user->Wallet
            ];
            
            $rs_data = curl_post($url_method, json_encode($data));
            

            if (isset($rs_data['login_url'])) {
                return redirect($rs_data['login_url']);
        
            } else {
                $errors = ['message' => 'เกมดังกล่าวอยู่ในระหว่างการบำรุงรักษา'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }
        
        // VA: 退出遊戲
        } else {

            $url_server = env('CURL_SERVER');

            // 先進行登入
            $url_method = $url_server . '/login';
            $data = [
                'user_id' => $user->MemberID
            ];
            $res = curl_post($url_method, json_encode($data));

            // 取得錢包餘額(VA端)
            $url_method = $url_server . '/wg/user/wallet/remote?language=thTH&token=' . $res['token'];
            $data = [];
            $response = curl_get($url_method, json_encode($data));

            // 退出遊戲確認VA錢包是否有餘額，有的話直接轉出
            if ( $response['point'] != 0 ) {
                if ($request->coin != 0 || null || ''){
                    $errors = ['message' => 'ไม่จำเป็นต้องใส่จำนวนเพื่อออกจากเกม'];
                    return redirect('/va_list')
                        ->withErrors($errors)
                        ->withInput();
                }
                
                $url_method = $url_server . '/wg/user/wallet/change?language=thTH&token=' . $res['token'];
                $coin_type  = $request->coinType;
                $wallet_rs  = $user->Wallet;
                // 金幣
                if ($coin_type == '1'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->GoldCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 銀幣
                if ($coin_type == '2'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->SilverCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                // 活動幣
                if ($coin_type == '3'){
                    $data = [
                        'point'  => -1*$response['point']/4,
                        'wallet' => $wallet_rs->ActivityCoin,
                    ];
                
                    $change_res = curl_post($url_method, json_encode($data));
                }
                    
                // 使用資料庫預存 - 退出遊戲，並將VA錢包餘額帶入
                $rs = DB::select('CALL sp_Front_Game_LeaveGame(?,?,?,?,?)', [
                    $user->MemberID,
                    $id,
                    $request->coinType,
                    intval($response['point'])/4 * 1000,
                    now()
                ]);

                $errors = [];
                if ($rs[0]->ResultCode != 0) {
                    if ($rs[0]->ResultCode == 1) {
                        $errors = ['message' => 'ไม่มีสมาชิก'];
                    }
                    if ($rs[0]->ResultCode == 2) {
                        $errors = ['message' => 'ไม่มีเกม'];
                    }
                    if ($rs[0]->ResultCode == 3) {
                        $errors = ['message' => 'ไม่มีสกุลเงิน'];
                    }
                    if ($rs[0]->ResultCode == 4) {
                        $errors = ['message' => 'ไม่มีความสมดุลที่ผิดปกติ'];
                    }
                    if ($rs[0]->ResultCode == 5) {
                        $errors = ['message' => 'ยอดเหรียญทองไม่เพียงพอ'];
                    }
                    if ($rs[0]->ResultCode == 6) {
                        $errors = ['message' => 'เหรียญไม่เพียงพอ'];
                    }
                    if ($rs[0]->ResultCode == 7) {
                        $errors = ['message' => 'ยอดเงินเหรียญเล่นเกมไม่เพียงพอ'];
                    }
                    if ($rs[0]->ResultCode == 8) {
                        $errors = ['message' => 'ยังอยู่ในเกม'];
                    }
                    if ($rs[0]->ResultCode == 9) {
                        $errors = ['message' => 'การทำธุรกรรมล้มเหลว'];
                    }
            
                    return redirect('/va_list')
                            ->withErrors($errors)
                            ->withInput();
                }
                $errors = ['message' => 'OK'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }else{
                // 當VA錢包餘額為0
                $errors = ['message' => 'ยอดเงินคงเหลือในวิดีโอเกมคือ 0'];
                return redirect('/va_list')
                    ->withErrors($errors)
                    ->withInput();
            }
        }
    }    
}
